#==============================================================================
SHELL   = zsh
#------------------------------------------------------------------------------
CC      = gfortran
LD      = gfortran
CFLAGS  = -O2 -Wall -Wextra
CFLAGS += -Wno-unused-parameter -Wno-unused-function -Wno-unused-result
BIN_DIR = bin
BLD_DIR = build
DAT_DIR = models
OUT_DIR = out
SRC_DIR = src
TST_DIR = tests
SRC     = $(wildcard $(SRC_DIR)/*.for)
OBJS    = $(patsubst $(SRC_DIR)/%.c,$(BLD_DIR)/%.o,$(SRC))
DEPS    = $(patsubst $(BLD_DIR)/%.o,$(BLD_DIR)/%.d,$(OBJS))
INPUT   = model_1 model_2
PROGRAM = relax4
#------------------------------------------------------------------------------
FILTERS = -F pandoc-crossref -F pandoc-include-code -F pandoc-fignos
OPTIONS = --template=styles/template.tex $(FILTERS)
CONFIG  = --metadata-file config.yml
BIB     = --filter pandoc-citeproc --bibliography=references.bib
SCT     = $(shell ls $(SCT_DIR)/**/*.md)
SCT_DIR = sections
REPORT  = report
#==============================================================================

.DEFAULT_GOAL = pdf

.PHONY: all pdf build setup clean run test

all: build run pdf

$(BLD_DIR)/%.d: %.c
	$(CC) -M $(CFLAGS) $(INCLUDES) $< -o $@

$(BLD_DIR)/%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

$(BIN_DIR)/$(PROGRAM): $(DEPS) $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS)

build: setup $(BIN_DIR)/$(PROGRAM)

run: build
	@for IN in $(INPUT) ; do \
		echo "Running relax and lp for $$IN" ; \
		./$(BIN_DIR)/$(PROGRAM) <$(DAT_DIR)/"$$IN".txt > $(OUT_DIR)/"$$IN".out; \
		lp_solve $(DAT_DIR)/"$$IN".lp > $(OUT_DIR)/"$$IN"_lp.out; \
		echo ""; \
	done

pdf:
	pandoc $(CONFIG) $(OPTIONS) $(BIB) -s $(SCT) -o $(REPORT).pdf

test:
	@echo "Write some tests!"

setup:
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(BLD_DIR)
	@mkdir -p $(OUT_DIR)

clean:
	@echo "Cleaning..."
	@echo ""
	@-cat .art/maid.ascii
	@-rm -rf $(BLD_DIR)/* $(BIN_DIR)/* $(OUT_DIR)/*
	@rm $(REPORT).pdf
	@echo ""
	@echo "...✓ done!"
