# Rede do problema de transporte resultante da mudança de variável {#sec:net}

![Rede do problema de transporte](figures/rede_transporte.png){ #fig:rede_transporte }

Na @fig:rede_transporte apresentam-se os vértices de excesso e defeito com o
respetivo fluxo (1 ou -1), sendo que no esquema os fluxos negativos são
representados com uma seta no sentido oposto ao da entrada no vértice. Nos
restantes vértices o valor do fluxo é 0.
