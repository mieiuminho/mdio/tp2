\part{}

# Sentido das ruas ABCDE {#sec:sentido_ruas}

Devemos começar por referir que o maior número de aluno do nosso grupo de
trabalho é o do aluno Hugo Filipe Duarte Carvalho (A85579).

Assim, de acordo com as indicações presentes no enunciado completamos o nosso
mapa, atribuindo o devido sentido às ruas B, C, D e E.

Consideremos o número antes referido na forma apresentada no enunciado (ABCDE):
podemos estabelecer a seguinte correspondência: A-8, B-5, C-5, D-7 e E-9. Como o
número que corresponde a B é ímpar a rua B é a subir. O número que corresponde a
C é 5 que é ímpar, portanto a rua C tem sentido para a esquerda. O número que
corresponde a D é 7, pelo que a rua é a subir. Por último, o número que
corresponde a E é 9 pelo que a rua E tem sentido para a esquerda.

![Mapa Completo](figures/mapa.png){ #fig:mapa height=330px }
