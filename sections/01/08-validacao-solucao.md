# Validação da solução {#sec:validation1}

Vamos primeiro analisar a solução sugerida pelo programa `Relax4`:

* $y_{12} = 2$
* $y_{24} = 0$
* $y_{41} = 1$
* $y_{44} = 0$
* $y_{23} = 1$
* $y_{36} = 1$
* $y_{65} = 1$
* $y_{56} = 0$
* $y_{67} = 0$
* $y_{73} = 0$
* $y_{58} = 0$
* $y_{87} = 0$
* $y_{74} = 0$
* $y_{38} = 0$
* $y_{81} = 0$

Para validarmos esta solução temos de confirmar que a mesma respeita as
instruções impostas pelo modelo desenvolvido.

Todos os $y_{ij}$ da solução proposta são iguais ou maiores que 0, pelo que a
primeira restrição do problema é respeitada.

Para a segunda série de restrições:

* $y_{12} - y_{81} - y_{41} = 1$

Como $y_{12} = 2$, $y_{81} = 0$ e $y_{41} = 1$, então $2 - 0 - 1 = 1$, pelo que
está satisfeita a restrição.

* $y_{42} + y_{23} - y_{12} = -1$

Como $y_{24} = 0$, $y_{23} = 1$ e $y_{12} = 2$, então $0 + 1 - 2 = -1$, pelo que
está satisfeita a restrição.

* $y_{36} + y_{38} - y_{23} - y_{73} = 0$

Como $y_{36} = 1$, $y_{38} = 0$, $y_{23} = 1$ e $y_{73} = 0$, pelo que $1 + 0 -
1 - 0 = 0$, pelo que está satisfeita a restrição.

* $y_{41} - y_{24} - y_{74} = 1$

Como $y_{41} = 1$, $y_{24} = 0$ e $y_{74} = 0$, pelo que $1 - 0 - 0 = 1$, pelo
que está satisfeita a restrição.

* $y_{58} + y_{56} - y_{65} = -1$

Como $y_{56} = 0$, $y_{56} = 0$ e $y_{65} = 1$, pelo que $0 + 0 - 1 = -1$, pelo
que está satisfeita a restrição.

* $y_{65} + y_{67} - y_{36} = 0$

Como $y_{65} = 1$, $y_{67} = 0$ e $y_{36} = 1$, pelo que $1 + 0 - 1 = 0$, pelo
que a restrição está satisfeita.

* $y_{73} + y_{74} - y_{67} - y_{87} = 0$

Como $y_{73} = 0$, $y_{74} = 0$, $y_{67} = 0$ e $y_{87} = 0$, pelo que $0 + 0 -
0 - 0 = 0$, pelo que a restrição está satisfeita.

* $y_{87} + y_{81} - y_{58} - y_{38} = 0$

Como $y_{87} = 0$, $y_{81} = 0$, $y_{58} = 0$ e $y_{38} = 0$, pelo que $0 + 0 -
0 - 0 = 0$, pelo que está satisfeita a restrição.

\cleardoublepage

