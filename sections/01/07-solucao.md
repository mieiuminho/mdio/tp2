# Solução Ótima {#sec:solution1}

![Caminhos de reposicionamento](figures/reposicionamento.png){ #fig:reposicionamento height=350px }

Nesta secção apresentaremos uma solução para o problema proposto. Importa
referir que esta solução não é única, podiam ser escolhidas maneiras diferentes
de realizar o percurso. Por forma a facilitar a apresentação da solução iremos
dividi-la em iterações. Vamos considerar uma iteração como sendo um sub-percurso
com início e fim no depósito de lixo.

Esta é apenas uma rota possível uma vez que poderíamos arranjar outras soluções
para o problema: basta pensarmos que podemos rearranjar a ordem em que acontecem
as iterações consideradas e podem ainda ser criadas iterações com diferentes
arranjos de arcos (pequenas variantes da solução que aqui apresentamos, sendo
que o valor da função objetivo não se altera).

De facto, um percurso como, por exemplo, o determinado no trabalho prático
número 1 pode ser descomposto em dois componentes, o primeiro de travessia de
arcos para assegurar que todos os arcos são visitados (cada arco é atravessado
uma vez nesta fase) e o segundo de travessia (fase em que são formados os
caminhos) para reposicionamento entre vértices de excesso e vértices de defeito.

É importante referir, portanto, que a primeira parte não é variável, isto é, tem
de se passar por todos os vértices pelo menos uma vez. O problema que tentamos
resolver nesta parte responde precisamente à segunda parte (formar os caminhos
para reposicionamento entre vértices de excesso e vértices de defeito).

Consideremos os custos de ambas as fases:

* 1ª fase: todas as variáveis de decisão tomam o valor 1. Pelo que o custo desta
corresponde a: 3 + 7 + + 9 + 5 + 16 + 12 + 12 + 8 + 7 + 2 + 3 + 10 + 8 + 14 + 4
= 120.

* 2ª fase: nesta fase existem apenas 5 variáveis de decisão que tomam um valor
diferente de 0, assim como passamos a mostrar: $y_{12} = 2$, $y_{41} = 1$,
$y_{23} = 1$, $y_{36} = 1$ e $y_{65} = 1$. Assim o custo desta fase é dado por:
2 * 3 + 12 + 7 + 5 + 2 = 6 + 12 + 7 + 5 + 2 = 32.

Assim o custo total das 2 fases é: 120 + 32 = 152.

Como o custo desta solução é exatamente igual ao da solução do problema do
primeiro trabalho prático decidimos copiar a solução anterior.

Deste modo, todos os caminhos ótimos são aqueles que obedecem a este método,
passar por todos os arcos 1 vez e passar mais 2 vezes pelo arco `12`, mais uma
vez pelo arco `41`, mais uma vez pelo arco `23`, mais uma vez pelo arco `36` e
mais uma vez pelo arco `65`.

Ordem pela qual são visitados os arcos na iteração 1:

$$12 \rightarrow 23 \rightarrow 36 \rightarrow 65 \rightarrow 56 \rightarrow 65
     \rightarrow 58 \rightarrow 87 \rightarrow 74 \rightarrow 44
     \rightarrow 41$$

![Iteração nº 1](figures/iteracao-1.png){ #fig:iteracao-1 height=350px }

\newpage

Ordem pela qual são visitados os arcos na iteração 2:

$$12 \rightarrow 24 \rightarrow 41$$

![Iteração nº 2](figures/iteracao-2.png){ #fig:iteracao-2 height=350px }

Ordem pela qual são visitados os arcos na iteração 3:

$$12 \rightarrow 23 \rightarrow 36 \rightarrow 67 \rightarrow 73 \rightarrow 38
     \rightarrow 81$$

![Iteração nº 3](figures/iteracao-3.png){ #fig:iteracao-3 height=350px }
