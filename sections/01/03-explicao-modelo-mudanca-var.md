# Explicação do modelo após a mudança de variável {#sec:model_after_var_shift}

Na mudança de variável sugerida, o limite inferior de fluxo no arco $(i, j)$ é 1
para todos os arcos transitáveis no problema proposto. Assim, respeitando a
mudança de variável imposta no enunciado sabemos que: $y_{ij} = x_{ij} -
l_{ij}$, no entanto, conforme já sugerimos: $l_{ij} = 1$ para todos os arcos
considerados neste problema, uma vez que o camião tem de visitar todos os arcos.
Desta forma, manipulando a equação, obtemos: $x_{ij} = y_{ij} + 1$. Manipulando
o modelo apresentado na última iteração deste trabalho prático (TP1):

Aplicando a mudança de variável obtemos a seguinte função objetivo:

$min$ $z'$ $= 3 y_{12} +  7 y_{23} +  9 y_{24} +  5 y_{36} + 16 y_{38}
                       + 12 y_{41} + 12 y_{44} +  8 y_{56} +  7 y_{58}
                       +  2 y_{65} +  3 y_{67} + 10 y_{74} +  8 y_{73}
                       + 14 y_{81} +  4 y_{87} + 120$

Aplicando a mudança de variável às condições que existiam no modelo anterior:

$y_{12} >= 0$; $y_{23} >= 0$; $y_{24} >= 0$; $y_{36} >= 0$; $y_{38} >= 0$;
$y_{41} >= 0$; $y_{44} >= 0$; $y_{56} >= 0$; $y_{58} >= 0$; $y_{65} >= 0$;
$y_{67} >= 0$; $y_{74} >= 0$; $y_{73} >= 0$; $y_{81} >= 0$; $y_{87} >= 0$;

Quanto às restrições relativas ao fluxo, ficamos com:

$y_{12} - y_{41} - y_{81} = 1$

$y_{23} + y_{24} - y_{12} = (-1)$

$y_{36} + y_{38} - y_{23} - y_{73} = 0$

$y_{41} - y_{24} - y_{74} =  1$

$y_{56} + y_{58} - y_{65} =  (-1)$

$y_{65} + y_{67} - y_{36} - y_{56} = 0$

$y_{74} + y_{73} - y_{87} - y_{67} = 0$

$y_{87} + y_{81} - y_{58} - y_{38} = 0$


