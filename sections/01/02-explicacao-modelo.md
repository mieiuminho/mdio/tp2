# Explicação do modelo {#sec:model1}

A estratégia que utilizamos para modelar o problema foi estabelecer um conjunto
de vértices, que representam os pontos em que pode ser tomada uma decisão em
relação à direção a seguir.

![Mapa com vértices](figures/mapa-vertices.png){ #fig:mapa-vertices height=280px }

Para modelar o problema precisamos de variáveis de decisão, pelo que podemos
usar a seguinte regra de nomenclatura para as mesmas: $x_{ij}$, que simbolizam
arcos, em que $i$ é o vértice em que se inicia o arco e $j$ o vértice onde o
mesmo termina.

No entanto, é importante clarificar o raciocínio que nos levou a determinar os
vértices da forma que apresentamos acima. Um vértice, no contexto do problema,
será um ponto onde o condutor do camião teria a possibilidade de escolher dois
caminhos diferentes, respeitando o sentido das ruas (não conduzindo em
contramão). Desta forma, obtemos apenas os vértices apresentados na
@fig:mapa-vertices.

![Mapa com os vértices excluídos](figures/mapa-excluded-vertices.png){ #fig:mapa-excluded-vertices height=280px }

Na @fig:mapa-excluded-vertices são apresentados a vermelhos os vértices que
são excluídos, uma vez que, no contexto do problema, nenhum deles implica uma
decisão, ou seja, só existe um caminho possível de modo a que o camionista se
mantenha no sentido correto.

Sabemos que do modelo de programação linear têm de constar uma função objetivo e
as condições a que a mesma está sujeita. Assim, perante o contexto do problema
apresentado percebemos que a função objetivo é aquela que espelha a distância
percorrida pelo camionista.

Reparamos que no trabalho prático anterior nos equivocamos no custo do arco
`44`, assim como no custo do arco `56` pelo que apresentamos agora o modelo
com esses valores devidamente corrigidos. Convém referir que o valor da
função objetivo da solução ótima do trabalho anterior se altera, passando a
ser: 152. Passaremos a considerar neste trabalho o modelo correto.

De acordo com a regra que estabelecemos para a nomenclatura das variáveis de
decisão temos a seguinte função objetivo:

$min$ $z$ $= 3 x_{12} +  7 x_{23} +  9 x_{24} +  5 x_{36} + 16 x_{38}
                      + 12 x_{41} + 12 x_{44} +  8 x_{56} +  7 x_{58}
                      +  2 x_{65} +  3 x_{67} + 10 x_{74} +  8 x_{73}
                      + 14 x_{81} +  4 x_{87}$

Relativamente às condições: sabemos que um dos objetivos do problema é que sejam
visitados todos os troços da rede de caminhos, com efeito, torna-se óbvio que o
valor de todas as variáveis de decisão numa solução admissível para o problema
tem de ser maior ou igual a 1.

$x_{12} >= 1$; $x_{23} >= 1$; $x_{24} >= 1$; $x_{36} >= 1$; $x_{38} >= 1$;
$x_{41} >= 1$; $x_{44} >= 1$; $x_{56} >= 1$; $x_{58} >= 1$; $x_{65} >= 1$;
$x_{67} >= 1$; $x_{74} >= 1$; $x_{73} >= 1$; $x_{81} >= 1$; $x_{87} >= 1$;

Temos obrigatoriamente de garantir que o circuito é possível, isto é, partir
do depósito e percorrer todas as ruas voltando à posição inicial. Temos
também de garantir que cada vez que visitamos um vértice saímos do mesmo,
para isso basta que forcemos o número de vezes que entramos num vértice a ser
igual ao número de vezes que saímos do mesmo. Importa referir que o arco 44
não necessita de condição que pondere esta situação, uma vez que se parte do
vértice 4 para ir ter ao vértice 4. Assim surgem as seguintes condições:

$x_{12} = x_{81} + x_{41}$

$x_{23} + x_{24} = x_{12}$

$x_{36} + x_{38} = x_{23} + x_{73}$

$x_{41} + x_{44} = x_{24} + x_{74} + x_{44}$

$x_{56} + x_{58} = x_{65}$

$x_{65} + x_{67} = x_{36} + x_{56}$

$x_{74} + x_{73} = x_{87} + x_{67}$

$x_{87} + x_{81} = x_{58} + x_{38}$
