\part{}

# Grafo bipartido {#sec:model2}

O problema será balenceado se atender às restrições de fluxo de todos os
vértices. Vamos de seguida recordá-la se passar todas as incógnitas para o lado
esquerdo, configurando assim um balanço do número de vezes que se entra e sai
de cada vértice.

![Grafo bipartido](figures/grafo_bipartido.png){ #fig:grafo_bipartido height=120px }

* 1ª restrição:

$y_{12} + 1 = y_{81} + 1 + y_{41} + 1 \Leftrightarrow y_{12} - y_{81} - y_{41} = 1$

* 2ª restrição:

$y_{24} + 1 + y_{23} + 1 = y_{12} + 1 \Leftrightarrow y_{24} + y_{23} - y_{12} = -1$

* 3ª restrição:

$y_{36} + 1 + y_{38} + 1 = y_{23} + 1 + y_{73} + 1 \Leftrightarrow y_{36} + y_{38} - y_{23} - y_{73} = 0$

* 4ª restrição:

$y_{41} + 1 = y_{24} + 1 + y_{74} + 1 \Leftrightarrow y_{41} - y_{24} - y_{74} = 1$

* 5ª restrição:

$y_{58} + 1 + y_{56} + 1 = y_{65} + 1 \Leftrightarrow y_{58} + y_{56} - y_{65} = - 1$

* 6ª restrição:

$y_{65} + 1 + y_{67} + 1 = y_{36} + 1 + y_{56} + 1 \Leftrightarrow y_{65} + y_{67} - y_{36} - y_{56} = 0$

* 7ª restrição:

$y_{73} + 1 + y_{74} + 1 = y_{67} + 1 + y_{87} + 1 \Leftrightarrow y_{73} + y_{74} - y_{67} - y_{87} =  0$

* 8ª restrição:

$y_{87} + 1 + y_{81} + 1 = y_{58} + 1 + y_{38} + 1 \Leftrightarrow y_{87} + y_{81} - y_{58} - y_{38} = 0$

Assim, se somarmos todos os lados direitos destas equações (relembremos que
cada equação reflete o balanço do número de vezes que entramos e saímos em
cada vértice) verificamos que o total é zero (0) pelo que o problema é
balanceado.
