# Matriz caminhos mais curtos {#sec:ex2pt2}

Deve-se referir que os resultados a seguir apresentados foram obtidos
inspecionando o esquema, descobrindo desta forma o caminho mais curto entre
os vértices de excesso (`1` e `4`) e os vértices de defeito (`2` e `5`).

|       | **1** | **4** |
|:-----:|:-----:|:-----:|
| **2** |   3   |   15  |
| **5** |   17  |   29  |

: Matriz dos caminhos mais curtos. {#tbl:matriz_caminhos_curtos}
