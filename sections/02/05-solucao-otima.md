# Solução Ótima {#sec:solution2}

O conjunto dos caminhos ótimos é aquele que passa por todos os arcos 1 vez.
Depois resolvemos o problema modelado nesta parte de modo a descobrir quais os
arcos dos vértices de excesso para os vértices de defeito e quantas vezes usar
cada um. Conforme o output do `Relax4` constatamos que: $y_{14} = 1$ e $y_{23}
= 1$.

De facto, um percurso como, por exemplo, o determinado no trabalho prático
número 1 pode ser descomposto em dois componentes, o primeiro de travessia de
arcos para assegurar que todos os arcos são visitados (cada arco é atravessado
uma vez nesta fase) e o segundo de travessia (fase em que são formados os
caminhos) para reposicionamento entre vértices de excesso e vértices de defeito.

Os valores associados aos arcos $y_{14}$ e $y_{23}$ são os que minimizam o custo
dos mesmos (caminho mais curtos entre os vértices dos extremos de cada arco), 15
e 17 respetivamente (conforme apresentado na tabela acima).

Calculando o custo correspondente às 2 fases:

* 1ª fase: mantém-se (120).

* 2ª fase: 15 + 17 = 32.

Assim, o custo total das fases é: 120 + 32 = 152.

Como o valor do custo da solução obtida é o mesmo do primeiro trabalho prático e
também do exercício anterior, então a solução mantém-se.
