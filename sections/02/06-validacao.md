# Validação da solução {#sec:validation2}

Para que possamos validar a solução obtida precisamos primeiro de perceber quais
os valores sugeridos pelo output do programa `Relax4` e precisamos de apresentar
o modelo do problema. Vamos então apresentar o modelo do problema (sendo que é
um problema de transporte em grafos bipartidos), razão pela qual a função
objetivo toma esta forma:

$min z: c_{12}x_{12} + c_{15}x_{15} + c_{42}x_{42} + c_{45}x_{45}$

Relembremos que os vértices de excesso são `1` e `4` e os vértices de defeitos
são `2` e `5`. Desta forma, surgem as seguintes restrições:

$x_{12} + x_{15} = 1$ e $x_{42} + x_{45} = 1$

O output do `Relax4` sugere: $x_{15} = 1$, $x_{42} = 1$, $x_{12} = 0$ e $x_{45}
= 0$.

Sabemos que se o resultado respeitar as restrições impostas pelo modelo a
solução e válida, portanto vamos verificar se tal condição é respeitada:

Como $x_{15} = 1$ e $x_{42} = 1$:

* 1ª condição:

$x_{12} + x_{15} = 1 \Leftrightarrow$

$\Leftrightarrow 0 + 1 = 1 \Leftrightarrow$

$\Leftrightarrow 1 = 1$

* 2ª condição

$x_{42} + x_{45} = 1 \Leftrightarrow$

$\Leftrightarrow 1 + 0 = 1 \Leftrightarrow$

$\Leftrightarrow 1 = 1$
